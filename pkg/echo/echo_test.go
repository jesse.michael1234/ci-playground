package echo

import "testing"

const (
	lll = "Okay, is everybody clear? We're gonna pick it up and move it. All we need is teamwork, okay? We're gonna lift the car... and slide it out. Lift... and slide. Rules are good, rules control the fun! You think that's what's weird? Joey, the man's been captain of a cereal for the last 40 years. Fine! Judge all you want but: married a lesbian, left a man at the alter, fell in love with a gay ice dancer, threw a girl's wooden leg in a fire, LIVES IN A BOX! Chandler. I sensed it was you. Man this is weird. You ever realize Captain Crunch's eyebrows are actually on his hat? I tell you, when I actually die, some people are gonna get seriously haunted. Raspberries? Good. Ladyfingers? Good. Beef? GOOD!"
)

func TestEcho(t *testing.T) {
	tests := []struct {
		name string
		arg  string
		want string
	}{
		{name: "regular string", arg: "regular", want: "regular"},
		{name: "empty string", arg: "", want: ""},
		{name: "long string", arg: lll, want: lll},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Echo(tt.arg); got != tt.want {
				t.Errorf("Echo() = %v, want %v", got, tt.want)
			}
		})
	}
}
