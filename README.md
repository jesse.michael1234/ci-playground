# CI Playground 🎢 


## 🏷 Automatic Tagging

Uses [Semantic-Release](https://semantic-release.gitbook.io/semantic-release/) to parse commit messages, determine semantic version, and automatically tag the release on commits to master.

In addition to automatically tagging the release, It updates the [CHANGELOG.md](CHANGELOG.md) with the [analyzed commit messages](https://github.com/semantic-release/commit-analyzer), which clearly shows changes made to the project.

Semantic release parses the commit messages in the [Angular Commit Message Format](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines) and constructs a version based on the commit message types (feat, fix,...).

Uses a [docker image](https://gitlab.com/jesse.michael1234/semantic-release) of semantic release that is configured to match the use case of this project and others like it.

Requires a `GITLAB_TOKEN` set as a protected variable in Gitlab CI settings.


## 📑 CI Templates

[Include](https://docs.gitlab.com/ee/ci/yaml/#include) Gitlab CI files from other projects to share CI across projects, require less copy/paste, and add/update CI quickly.

This project shows this work by using Go build/test stages from [ci-templates/go-ci.yml](https://gitlab.com/jesse.michael1234/ci-templates/-/blob/master/go-ci.yml) and the semantic release step from [ci-templates/release-ci.yml](https://gitlab.com/jesse.michael1234/ci-templates/-/blob/master/release-ci.yml).

It even overrides a variable `COVERPKG` set in the templates to customize the usage for this project.
