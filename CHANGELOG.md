## [2.3.1](https://gitlab.com/jesse.michael1234/ci-playground/compare/v2.3.0...v2.3.1) (2020-03-06)


### Bug Fixes

* trigger golint ([49cf789](https://gitlab.com/jesse.michael1234/ci-playground/commit/49cf7890d32f67513d6b12fe2e1e720c4e1f67d6))
* use imported golang ci ([718eec3](https://gitlab.com/jesse.michael1234/ci-playground/commit/718eec33c015e44caed70e6ab45f6993d9a77eb4))

# [2.3.0](https://gitlab.com/jesse.michael1234/ci-playground/compare/v2.2.0...v2.3.0) (2020-03-06)


### Features

* attempt to use ci templates ([c32bf3b](https://gitlab.com/jesse.michael1234/ci-playground/commit/c32bf3baf05fae9a5028cd011047c132e7bf9de1))

# [2.2.0](https://gitlab.com/jesse.michael1234/ci-playground/compare/v2.1.0...v2.2.0) (2020-03-05)


### Features

* remove docs from wrong location ([81c650d](https://gitlab.com/jesse.michael1234/ci-playground/commit/81c650df881a6455f9d55423cc62b2dd77b62db7))

## [2.0.1](https://gitlab.com/jesse.michael1234/ci-playground/compare/v2.0.0...v2.0.1) (2020-03-04)


### Bug Fixes

* patch the version ([f03ba05](https://gitlab.com/jesse.michael1234/ci-playground/commit/f03ba053e96e001a7e01c18e2325754e303cf9bc))
* update readme ([381178c](https://gitlab.com/jesse.michael1234/ci-playground/commit/381178c018edc73e6ed10fcd1fbc70115ce98c00))

# [2.0.0](https://gitlab.com/jesse.michael1234/ci-playground/compare/v1.2.0...v2.0.0) (2020-03-04)


### Features

* breaking in the footer ([05ae036](https://gitlab.com/jesse.michael1234/ci-playground/commit/05ae0369f4bbd375a4daa6763bdb0790f5074326))


### BREAKING CHANGES

* needs to be in the footer..... the footer...

# [1.2.0](https://gitlab.com/jesse.michael1234/ci-playground/compare/v1.1.0...v1.2.0) (2020-03-04)


### Features

* update readme ([083ffdf](https://gitlab.com/jesse.michael1234/ci-playground/commit/083ffdfc52f077a725554d8481927148e2b39b0e))
* update text ([5825c5e](https://gitlab.com/jesse.michael1234/ci-playground/commit/5825c5e342797d804f4d7dccd3241e9c9d7d895d))
